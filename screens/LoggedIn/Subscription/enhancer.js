import { connect } from 'react-redux'
import { compose, withStateHandlers, withHandlers } from 'recompose'
import {
    firebaseConnect,
    isEmpty,
    isLoaded,
    populate
} from 'react-redux-firebase'
import { spinnerWhileLoading } from '@components'
import { unsubscribe } from '@api'
import { Alert } from 'react-native'

export default compose(
    firebaseConnect((state, props) => {
        return ['plans']
    }),
    connect(({ firebase, firebase: { data, auth } }) => ({
        auth,
        subscription: data.subscriptions ? data.subscriptions[auth.uid] : null,
        plans: data.plans && data.plans
    })),
    withStateHandlers(
        {
            cancelling: false,
            cancellingResultMsg: null,
            openCancelDialog: false
        },
        {
            toggleCancelling: ({ cancelling }) => (result = null) => ({
                cancelling: !cancelling,
                cancellingResultMsg: result
            }),
            toggleCancelDialog: ({ openCancelDialog }) => () => ({
                openCancelDialog: !openCancelDialog,
                cancellingResultMsg: null
            })
        }
    ),
    withHandlers({
        cancelSubscription: ({ toggleCancelling, auth }) => async () => {
            toggleCancelling()
            let result = await unsubscribe({ userId: auth.uid })

            toggleCancelling(result.data ? result.data.msg : null)
        }
    }),
    spinnerWhileLoading(['auth'])
)
