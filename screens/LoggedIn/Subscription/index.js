import React from 'react'
import { Content, View, Button, Text, Badge } from 'native-base'
import { StyleSheet, ActivityIndicator } from 'react-native'
import { images, GlobalStyles as styles } from '@components'
import enhancer from './enhancer'
import { PageDivider } from '@theme/base'
import withLoader from '@components/hoc/withLoader'
import { PricingCard, Card } from 'react-native-elements'
import * as Progress from 'react-native-progress'
import { getSubscriptionInfo } from '@helpers/functions'
import BaseScreen from '@components/BaseScreen'
import colors from '@theme/colors'
import withPaymentHandler from '@components/hoc/withPaymentHandler'
import Dialog from 'react-native-dialog'

const getCurrentPlanName = subscriptionType => {
    switch (subscriptionType) {
        case 'none':
            return 'No seleccionado'
        case 'free':
            return 'Gratis'
        case 'premium':
            return 'Premium'
        default:
            return ''
    }
}

let Subscription = ({
    navigation: { navigate },
    auth,
    subscription,
    plans,
    setOpenStripeCheckout,
    cancelling,
    cancellingResultMsg,
    cancelSubscription,
    toggleCancelDialog,
    openCancelDialog
}) => (
    <BaseScreen>
        <Text
            uppercase
            style={[
                styles.pageTitle,
                styles.alignCenterSelf,
                { marginTop: 20 },
                styles.secondaryText
            ]}>
            Suscripción
        </Text>

        <Card
            title="Detalle"
            titleStyle={[styles.secondaryText]}
            containerStyle={{
                backgroundColor: colors.PRIMARY_LIGHT_2,
                borderColor: colors.PRIMARY_LIGHT_2
            }}>
            <Text style={[styles.whiteTxt]}>Email: {auth.email}</Text>
            <Text style={[styles.whiteTxt]}>
                Plan:{' '}
                {getCurrentPlanName(subscription ? subscription.type : null)}
            </Text>

            {((subscription && subscription.type === 'premium') ||
                (subscription && subscription.startDate)) && (
                <View style={{ marginTop: 20 }}>
                    <Text style={[styles.whiteTxt]}>
                        Periodo: {subscription.startDate} -{' '}
                        {subscription.endDate}{' '}
                    </Text>

                    <View style={{ marginTop: 10, marginBottom: 10 }}>
                        {subscription && (
                            <Progress.Bar
                                progress={
                                    getSubscriptionInfo(subscription)
                                        .percentageRemaining
                                }
                                width={200}
                            />
                        )}
                        <Text style={[styles.whiteTxt]}>
                            {getSubscriptionInfo(subscription).remaining} dias
                            faltantes
                        </Text>
                    </View>

                    {subscription.cancelsAt && (
                        <Badge danger>
                            <Text style={{ fontSize: 10 }}>
                                Se cancela al final del periodo
                            </Text>
                        </Badge>
                    )}
                </View>
            )}
            <View style={{ marginTop: 20 }}>
                {subscription &&
                    subscription.type === 'premium' &&
                    !subscription.cancelsAt && (
                        <Button
                            small
                            onPress={() => {
                                toggleCancelDialog()
                            }}>
                            <Text>Cancelar suscripción</Text>
                        </Button>
                    )}
            </View>
        </Card>
        <Dialog.Container visible={openCancelDialog}>
            <Dialog.Title>Cancelar suscripción</Dialog.Title>
            <Dialog.Description>
                {cancelling
                    ? 'Cancelando tu suscripción...'
                    : !cancellingResultMsg
                    ? 'Estas seguro que deseas cancelar tu suscripción?. Nota: Según nuestros términos y condiciones, pueden haber cobros por penalización por compromiso de permanencia.'
                    : cancellingResultMsg}
            </Dialog.Description>
            {!cancelling && (
                <Dialog.Button
                    label={!cancellingResultMsg ? 'No' : 'Cerrar'}
                    onPress={() => toggleCancelDialog()}
                />
            )}
            {!cancelling && !cancellingResultMsg && (
                <Dialog.Button
                    onPress={() => cancelSubscription()}
                    label="Cancelar mi suscripción"
                />
            )}
        </Dialog.Container>
        <View>
            {plans && (
                <React.Fragment>
                    <PricingCard
                        containerStyle={{
                            backgroundColor: colors.PRIMARY_LIGHT_2,
                            borderColor: colors.PRIMARY_LIGHT_2
                        }}
                        color="#4f9deb"
                        pricingStyle={{
                            color: 'white'
                        }}
                        title={`${plans['free'].name}`}
                        price={`€${plans['free'].price}/mes`}
                        info={JSON.parse(plans['free'].description)}
                        button={{
                            title: '',
                            icon: 'flight-takeoff',
                            buttonStyle: {
                                display: 'none'
                            }
                        }}
                        onButtonPress={() => {}}
                    />
                    <PricingCard
                        containerStyle={{
                            backgroundColor: colors.PRIMARY_LIGHT_2,
                            borderColor: colors.PRIMARY_LIGHT_2
                        }}
                        pricingStyle={{
                            color: 'white'
                        }}
                        color={colors.SECONDARY}
                        title={`${plans['premium'].name}`}
                        price={`€${plans['premium'].price}/mes`}
                        info={JSON.parse(plans['premium'].description)}
                        button={{
                            title: 'COMPRAR',
                            icon: 'flight-takeoff',
                            buttonStyle: {
                                display:
                                    subscription &&
                                    subscription.type === 'premium'
                                        ? 'none'
                                        : null
                            }
                        }}
                        onButtonPress={() => {
                            setOpenStripeCheckout(true, {
                                prepopulatedEmail: auth.email,
                                amount: plans['premium'].price * 100,
                                description: 'Plan Premium',
                                planId: 'premium',
                                loadingMessage: 'Procesando tu pago...',
                                panelLabel: 'Pagar'
                            })
                        }}
                    />
                </React.Fragment>
            )}
        </View>
    </BaseScreen>
)
export default enhancer(withPaymentHandler(Subscription))
