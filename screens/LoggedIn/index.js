import React from 'react'
import {
    createMaterialTopTabNavigator,
    createStackNavigator
} from 'react-navigation'
import { Ionicons, FontAwesome } from '@expo/vector-icons'
import Home from './Home'
import Subscription from './Subscription'
import Settings from './Settings'
import SportPage from './SportPage'
import colors from '@theme/colors'

const HomeStack = createStackNavigator(
    {
        Home: {
            screen: Home
        },
        SportPage: {
            screen: SportPage,
            navigationOptions: {
                tabBarVisible: false
            }
        }
    },
    {
        defaultNavigationOptions: {
            header: null
        },
        initialRouteName: 'Home'
    }
)

HomeStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true

    if (navigation.state.routeName === 'SporPage') {
        tabBarVisible = false
    }

    return {
        tabBarVisible
    }
}

export default createMaterialTopTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: ({ navigation }) => ({
                tabBarLabel: 'Inicio',
                tabBarIcon: ({ tintColor }) => (
                    <FontAwesome name="home" size={23} color={tintColor} />
                )
            })
        },
        Subscription: {
            screen: Subscription,
            navigationOptions: ({ navigation }) => ({
                tabBarLabel: 'Suscripción',
                tabBarIcon: ({ tintColor }) => (
                    <FontAwesome
                        name="credit-card"
                        size={23}
                        color={tintColor}
                    />
                )
            })
        },
        Settings: {
            screen: Settings,
            navigationOptions: ({ navigation }) => ({
                tabBarLabel: 'Más',
                tabBarIcon: ({ tintColor }) => (
                    <FontAwesome name="bars" size={23} color={tintColor} />
                )
            })
        }
    },
    {
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
            activeTintColor: colors.SECONDARY,
            inactiveTintColor: colors.WHITE,
            indicatorStyle: {
                backgroundColor: colors.SECONDARY
            },
            style: {
                backgroundColor: colors.PRIMARY_LIGHT_3
            }
        }
    }
)
