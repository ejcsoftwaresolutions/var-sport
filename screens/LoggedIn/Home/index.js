import React from 'react'
import {
    List,
    Content,
    Thumbnail,
    View,
    Text,
    TouchableOpacity,
    Button
} from 'native-base'
import { images, GlobalStyles } from '@components'
import styles from './style'
import enhancer from './enhancer'
import { ListItem, Card } from 'react-native-elements'
import colors from '@theme/colors'
import { ScrollView, Image } from 'react-native'
import BaseScreen from '@components/BaseScreen'
import withPaymentHandler from '@components/hoc/withPaymentHandler'
import { Constants, AdMobBanner } from 'expo'

const Banner = props => {
    return !props.isPremium ? (
        <View
            style={{
                width: '100%',
                alignItems: 'center',
                position: 'absolute',
                left: 0,
                right: 0,
                bottom: 0
            }}>
            <AdMobBanner
                bannerSize="banner"
                //adUnitID="ca-app-pub-3940256099942544/6300978111"
                adUnitID="ca-app-pub-6826295524219358/1896975497" // Test ID, Replace with your-admob-unit-id
                testDeviceID={
                    Constants.isDevice ? Constants.deviceId : 'EMULATOR'
                    //'EMULATOR'
                }
                onDidFailToReceiveAdWithError={e => console.log(e)}
            />
        </View>
    ) : (
        <View />
    )
}

let Home = ({
    profile,
    auth,
    sports,
    subscription,
    navigation: { navigate },
    setOpenStripeCheckout,
    isPremium
}) => (
    <BaseScreen banner={() => <Banner isPremium={isPremium} />}>
        <Image
            style={{
                width: 150,
                height: 150,
                alignSelf: 'center',
                marginBottom: 20,
                marginTop: 20
            }}
            source={images.logo}
        />

        <List>
            {sports &&
                Object.values(sports).map(sport => (
                    <ListItem
                        key={sport.key}
                        Component={TouchableOpacity}
                        friction={90}
                        tension={100}
                        activeScale={0.95} //
                        linearGradientProps={{
                            colors: [
                                colors.PRIMARY_LIGHT_3,
                                colors.PRIMARY_LIGHT_2
                            ],
                            start: [1, 0],
                            end: [0.2, 0]
                        }}
                        leftAvatar={{
                            rounded: true,
                            source:
                                Object.keys(images).indexOf(
                                    sport.iconConstant
                                ) > -1
                                    ? images[sport.iconConstant]
                                    : null,
                            size: 'large'
                        }}
                        title={`${sport.name.toUpperCase()}`}
                        titleStyle={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 20
                        }}
                        chevronColor="white"
                        chevron
                        containerStyle={{
                            margin: 10,
                            minHeight: 100
                        }}
                        onPress={() => {
                            navigate('SportPage', {
                                sport: sport.key,
                                sportName: sport.name,
                                sportIcon: sport.iconConstant
                            })
                        }}
                    />
                ))}
        </List>
    </BaseScreen>
)

export default enhancer(withPaymentHandler(Home))
