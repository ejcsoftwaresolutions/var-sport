import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from '@components'
import { Constants, AdMobInterstitial } from 'expo'

export default compose(
    firebaseConnect((state, props) => {
        return ['sports']
    }),
    connect(({ firebase: { data, auth, profile } }) => {
        const subscription = data.subscriptions && data.subscriptions[auth.uid]
        return {
            auth,
            subscription: subscription,
            sports: data.sports && data.sports,
            isPremium: subscription && subscription.type === 'premium'
        }
    }),
    lifecycle({
        async componentDidMount() {}
    }),
    spinnerWhileLoading(['sports'])
)
