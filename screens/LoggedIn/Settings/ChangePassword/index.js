import React from 'react'
import {
  Content,
  Form,
  Item,
  Input,
  Thumbnail,
  View,
  Button,
  Text
} from 'native-base'
import { KeyboardAvoidingView, Image, ScrollView } from 'react-native'
import { images, GlobalStyles as styles } from '@components'
import enhancer from './enhancer'
import withLoader from '@components/hoc/withLoader'

const SubmitButton = withLoader(Button)

let ChangePassword = ({
  navigation: { navigate },
  updateForm,
  changePassword,
  isLoading
}) => (
  <Content
    contentContainerStyle={[styles.verticalAlignCenter, styles.pageBackground]}>
    <ScrollView>
      <Image
        style={{
          width: 150,
          height: 150,
          alignSelf: 'center',
          marginBottom: 20,
          marginTop: 20
        }}
        source={images.logo}
      />
      <KeyboardAvoidingView behavior="padding">
        <Form style={styles.marginH15}>
          <Item regular style={styles.formItem}>
            <Input
              placeholder="Contraseña actual"
              onChangeText={text => updateForm('oldpass', text)}
              placeholderTextColor="#b9b9b9"
              type="password"
              secureTextEntry
            />
          </Item>
          <Item regular style={styles.formItem}>
            <Input
              placeholder="Nueva contraseña"
              onChangeText={text => updateForm('newpass', text)}
              placeholderTextColor="#b9b9b9"
              type="password"
              secureTextEntry
            />
          </Item>
          <Item regular style={styles.formItem}>
            <Input
              placeholder="Confirmar contraseña"
              onChangeText={text => updateForm('confirmpass', text)}
              placeholderTextColor="#b9b9b9"
              type="password"
              secureTextEntry
            />
          </Item>
        </Form>
      </KeyboardAvoidingView>
      <SubmitButton
        isLoading={isLoading}
        block
        secondary
        style={[styles.marginH15, { marginTop: 15 }]}
        rounded
        onPress={changePassword}>
        <Text style={styles.whiteTxt}>Cambiar contraseña</Text>
      </SubmitButton>
    </ScrollView>
  </Content>
)
export default enhancer(ChangePassword)
