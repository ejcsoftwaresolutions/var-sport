import { compose, withHandlers, withStateHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { Alerts } from '@components'

export default compose(
  firebaseConnect(),
  withStateHandlers(
    {
      oldpass: '',
      newpass: '',
      confirmpass: '',
      isLoading: false,
    },
    {
      setIsLoading: () => (isLoading) => ({
        isLoading: isLoading
      }),
      updateForm: () => (field, value) => ({
        [field]: value
      })
    }
  ),
  withHandlers({
    changePassword: ({ firebase, oldpass, newpass, confirmpass, setIsLoading }) => () => {

      setIsLoading(true)

      let isInvalid = false
      if (oldpass == '') {
        isInvalid = true;
        return Alerts.danger('Por favor, coloca tu contraseña actual')
      }
      if (newpass == '') {
        isInvalid = true;
        return Alerts.danger('Por favor, coloca una nueva contraseña')
      }
      if (confirmpass == '') {
        isInvalid = true;
        return Alerts.danger('Por favor, verifica la contraseña')
      }
      if (newpass != confirmpass) {
        isInvalid = true;
        return Alerts.danger(
          'Las contraseñas deben coincidir.'
        )
      }

      if (isInvalid) setIsLoading(false)

      var user = firebase.auth().currentUser
      var cred = firebase.auth.EmailAuthProvider.credential(user.email, oldpass)
      user
        .reauthenticateWithCredential(cred)
        .then(() => {
          user
            .updatePassword(newpass)
            .then(() => {
              setIsLoading(false)
              Alerts.msg('Contraseña modificada exitosamente.')
            })
            .catch(error => {
              setIsLoading(false)
              Alerts.danger(error.message)
            })
        })
        .catch(() => {
          setIsLoading(false)
          Alerts.danger('Por favor, verifica tu contraseña actual')
        })
    }
  })
)
