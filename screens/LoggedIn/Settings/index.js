import { createStackNavigator } from 'react-navigation'
import Index from './Index'
import ChangePassword from './ChangePassword'

const SettingsStack = createStackNavigator(
  {
    Index: {
      screen: Index,
      navigationOptions: {
        header: null
      }
    },
    ChangePassword: {
      screen: ChangePassword,
      navigationOptions: {
        title: 'Cambiar contraseña',
        tabBarVisible: false,
      }
    }
  },
  {
    initialRouteName: 'Index'
  }
)

SettingsStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default SettingsStack
