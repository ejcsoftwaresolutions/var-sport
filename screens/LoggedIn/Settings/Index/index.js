import React from 'react'
import {
    Button,
    Content,
    Text,
    Thumbnail,
    View,
    List,
    ListItem,
    CardItem
} from 'native-base'
import { Linking, TouchableOpacity, ScrollView } from 'react-native'
import enhancer from './enhancer'
import { images, GlobalStyles as styles } from '@components'
import { appInfo } from '@config'
import BaseScreen from '@components/BaseScreen'
import { Card } from 'react-native-elements'
import colors from '@theme/colors'
import { Localization } from 'expo'

let Settings = ({ navigation: { navigate }, logout, auth, profile }) => (
    <BaseScreen>
        <Card
            title="Configuraciones generales"
            titleStyle={[styles.secondaryText]}
            containerStyle={[
                {
                    backgroundColor: colors.PRIMARY_LIGHT_2,
                    borderColor: colors.PRIMARY_LIGHT_2
                },
                styles.marginH15
            ]}>
            <Text style={[styles.whiteTxt, styles.marginV15]}>Zona horaria: {Localization.timezone}</Text>
            <TouchableOpacity
                button
                style={[styles.marginV15]}
                onPress={() => {
                    navigate('ChangePassword')
                }}>
                <Text style={[styles.whiteTxt]}>Cambiar contraseña</Text>
            </TouchableOpacity>
            <Button style={{ marginTop: 10 }} block onPress={logout}>
                <Text>Cerrar sesión</Text>
            </Button>
        </Card>

        <Card
            title="Información"
            titleStyle={[styles.secondaryText]}
            containerStyle={[
                {
                    backgroundColor: colors.PRIMARY_LIGHT_2,
                    borderColor: colors.PRIMARY_LIGHT_2
                },
                styles.marginH15
            ]}>
            <TouchableOpacity
                button
                onPress={() => Linking.openURL(appInfo.websiteUrl)}>
                <Text style={[styles.whiteTxt]}>{appInfo.websiteUrl}</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={[styles.whiteTxt, styles.m15]}>Versión</Text>
                <Text style={[styles.whiteTxt, styles.marginSetting]}>
                    {appInfo.version}
                </Text>
            </TouchableOpacity>
        </Card>
    </BaseScreen>
)

export default enhancer(Settings)
