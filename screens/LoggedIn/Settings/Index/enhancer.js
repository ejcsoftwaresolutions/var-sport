import { compose, withHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { Alerts, spinnerWhileLoading } from '@components'
import { connect } from 'react-redux'
import { logOut } from '@services/auth'

export default compose(
    firebaseConnect(),
    connect(({ firebase: { profile, auth } }) => ({
        profile,
        auth
    })),
    withHandlers({
        logout: ({ firebase }) => () =>
            logOut(firebase).catch(err => Alerts.danger(err.message))
    })
    // spinnerWhileLoading(['auth', 'profile'])
)
