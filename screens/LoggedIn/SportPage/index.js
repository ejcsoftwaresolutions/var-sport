import React from 'react'
import {
    Content,
    View,
    Button,
    Text,
    List,
    Body,
    Left,
    Right,
    Icon,
    Radio,
    ListItem as ListItemBasic
} from 'native-base'
import {
    TouchableOpacity,
    StyleSheet,
    ActivityIndicator,
    ScrollView,
    Modal,
    Image
} from 'react-native'
import enhancer from './enhancer'
import { ListItem, Card } from 'react-native-elements'

import colors from '@theme/colors'
import BaseScreen from '@components/BaseScreen'
import { GlobalStyles } from '@components'
import withPaymentHandler from '@components/hoc/withPaymentHandler'
import images from '@components/images'
import { Constants, AdMobBanner } from 'expo'

const styles = StyleSheet.create({
    eventProgram: {
        flexDirection: 'column'
    },
    eventProgramItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    eventProgramItemText: {
        fontSize: 11
    },
    eventTitle: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15
    },
    eventSubtitle: {
        fontSize: 11
    },
    channelsWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    mainBanner: {
        minHeight: 200,
        backgroundColor: colors.PRIMARY,
        alignItems: 'center',
        justifyContent: 'center'
    },
    alert: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1c1c1d6b'
    },
    alertWrapper: {
        padding: 15,
        backgroundColor: 'white',
        minWidth: 300,
        borderRadius: 10
    },
    mainBannerTitle: {
        color: 'white',
        fontSize: 20
        // flexDirection: 'row'
    },
    icon: {
        width: 100,
        height: 100
        // backgroundColor: 'red'
    }
})

const Banner = props => {
    return !props.isPremium ? (
        <View
            style={{
                width: '100%',
                alignItems: 'center',
                position: 'absolute',
                left: 0,
                right: 0,
                bottom: 0
            }}>
            <AdMobBanner
                bannerSize="banner"
                //adUnitID="ca-app-pub-3940256099942544/6300978111" 
                adUnitID="ca-app-pub-6826295524219358/1896975497" // Test ID, Replace with your-admob-unit-id
                testDeviceID={
                    Constants.isDevice ? Constants.deviceId : 'EMULATOR'
                    //'EMULATOR'
                }
                onDidFailToReceiveAdWithError={e => console.log(e)}
            />
        </View>
    ) : (
        <View />
    )
}

let SportPage = ({
    events,
    navigation: { navigate, getParam },
    openInPlayer,
    opening,
    playModalVisible,
    toggleModalPlayVisible,
    activeSportEvent,
    openingChannelKey,
    setOpenStripeCheckout,
    isPremium,
    getLocaleDateTime
}) => (
    <BaseScreen banner={() => <Banner isPremium={isPremium} />}>
        <View style={styles.mainBanner}>
            <View style={styles.icon}>
                <Image
                    style={styles.icon}
                    source={
                        Object.keys(images).indexOf(getParam('sportIcon')) > -1
                            ? images[getParam('sportIcon')]
                            : null
                    }
                />
            </View>
            <Text style={styles.mainBannerTitle}>{getParam('sportName')}</Text>
        </View>
        <List>
            {events.length > 0 ? (
                events.map((sportEvent, i) => {
                    let dateTime = getLocaleDateTime(sportEvent.day, sportEvent.time);
                    return (
                        <ListItem
                            key={i}
                            Component={TouchableOpacity}
                            friction={90}
                            tension={100}
                            activeScale={0.95} //
                            linearGradientProps={{
                                colors: [
                                    colors.PRIMARY_LIGHT_3,
                                    colors.PRIMARY_LIGHT_2
                                ],
                                start: [1, 0],
                                end: [0.2, 0]
                            }}
                            containerStyle={{
                                margin: 10,
                                minHeight: 100
                            }}
                            title={`${sportEvent.event}`}
                            titleStyle={styles.eventTitle}
                            subtitle={
                                <View>
                                    <Text note style={styles.eventSubtitle}>
                                        {sportEvent.competition}
                                    </Text>
                                    <View style={styles.channelsWrapper}>
                                        <Icon
                                            name="tv"
                                            type="FontAwesome"
                                            style={{
                                                fontSize: 11,
                                                color: colors.SECONDARY,
                                                marginRight: 5
                                            }}
                                        />
                                        <Text note style={styles.eventSubtitle}>
                                            {sportEvent.channels
                                                .map(
                                                    c =>
                                                        c.channel +
                                                        ' [' +
                                                        c.lang +
                                                        ']'
                                                )
                                                .join(' ')}
                                        </Text>
                                    </View>
                                </View>
                            }
                            chevronColor="white"
                            chevron
                            onPress={() => {
                                toggleModalPlayVisible(sportEvent)
                            }}
                            rightElement={
                                <View style={styles.eventProgram}>
                                    <View style={styles.eventProgramItem}>
                                        <Icon
                                            name="calendar"
                                            type="FontAwesome"
                                            style={{
                                                fontSize: 11,
                                                color: colors.SECONDARY,
                                                marginRight: 5
                                            }}
                                        />
                                        <Text
                                            note
                                            style={styles.eventProgramItemText}>
                                            {dateTime.date}
                                        </Text>
                                    </View>
                                    <View style={styles.eventProgramItem}>
                                        <Icon
                                            name="clock-o"
                                            type="FontAwesome"
                                            style={{
                                                fontSize: 11,
                                                color: colors.SECONDARY,
                                                marginRight: 5
                                            }}
                                        />
                                        <Text
                                            note
                                            style={styles.eventProgramItemText}>
                                            {dateTime.time}
                                        </Text>
                                    </View>
                                </View>
                            }
                        />
                    )
                })
            ) : (
                <Card
                    containerStyle={{
                        backgroundColor: colors.PRIMARY_LIGHT_2,
                        borderColor: colors.PRIMARY_LIGHT_2,
                        padding: 30
                    }}>
                    <Text
                        style={[
                            GlobalStyles.whiteTxt,
                            GlobalStyles.alignCenterSelf
                        ]}>
                        No hay eventos en programación en estos momentos, revisa
                        más tarde.
                    </Text>
                </Card>
            )}
        </List>
        <Modal animationType="fade" transparent visible={playModalVisible}>
            <View style={styles.alert}>
                <View style={styles.alertWrapper}>
                    <Text style={{ alignSelf: 'center' }}>
                        Selecciona el canal
                    </Text>

                    <List>
                        {activeSportEvent &&
                            activeSportEvent.channels &&
                            activeSportEvent.channels.map((c, i) => (
                                <ListItemBasic
                                    button
                                    onPress={
                                        !opening
                                            ? () => {
                                                  openInPlayer(
                                                      i,
                                                      c.channel,
                                                      setOpenStripeCheckout
                                                  )
                                              }
                                            : null
                                    }>
                                    <Left>
                                        <Text>
                                            {c.channel} - {c.lang}
                                        </Text>
                                    </Left>
                                    <Right>
                                        {opening && openingChannelKey === i ? (
                                            <ActivityIndicator
                                                color={colors.SECONDARY}
                                            />
                                        ) : (
                                            <Icon
                                                name="caretright"
                                                type="AntDesign"
                                            />
                                        )}
                                    </Right>
                                </ListItemBasic>
                            ))}
                    </List>

                    <Button
                        rounded
                        // disabled={opening}
                        success
                        small
                        style={{ marginTop: 10, alignSelf: 'center' }}
                        onPress={() => {
                            toggleModalPlayVisible(null, false)
                        }}>
                        <Text>Cancelar</Text>
                    </Button>
                </View>
            </View>
        </Modal>
    </BaseScreen>
)

export default enhancer(withPaymentHandler(SportPage))
