import { connect } from 'react-redux'
import { compose, lifecycle, withStateHandlers, withHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from '@components'
import { getGuide, getChannelLink, canPlay } from '@api'
import { Linking, Alert } from 'react-native'
import { LoadInterstitialAd } from '@services/ads'
import { Constants, AdMobInterstitial } from 'expo'
import moment from "moment";
import 'moment-timezone';

import { Localization } from 'expo'

const handleNotValidSubscription = (
    data,
    auth,
    plans,
    setOpenStripeCheckout
) => {
    const userSubscription = data.subscription
    const canActivateTrial =
        userSubscription.type === 'none' && !userSubscription.enjoyedTrial

    Alert.alert(
        'No tienes una suscripción activa',
        canActivateTrial
            ? 'Activa tu periodo de prueba por 7 dias totalmente GRATIS!'
            : 'Compra una suscripción para disfrutar de la mejor programación',
        [
            {
                text: 'Cerrar',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            },
            {
                text: canActivateTrial ? 'Activar' : 'Comprar',
                onPress: () => {
                    let order = {
                        prepopulatedEmail: auth.email
                    }

                    order = canActivateTrial
                        ? {
                              ...order,
                              amount: 0,
                              description: 'Periodo de prueba',
                              trialTarget: 'premium',
                              trial: true,
                              loadingMessage: 'Activando periodo de prueba...',
                              panelLabel: 'Activar'
                          }
                        : {
                              ...order,
                              amount: plans['premium'].price * 100,
                              description: 'Plan Premium',
                              planId: 'premium',
                              loadingMessage: 'Procesando tu pago...',
                              panelLabel: 'Pagar'
                          }

                    setOpenStripeCheckout(true, order)
                }
            }
        ],
        { cancelable: true }
    )
}

const handleOnlineRestriction = data => {
    const { plan } = data
    Alert.alert(
        'Maximo de usuarios en linea superado',
        'Tu plan tiene un maximo de usuarios por cuenta',
        [
            {
                text: 'Ok',
                style: 'cancel'
            }
        ]
    )
}

export default compose(
    firebaseConnect((state, props) => {}),
    connect(({ firebase: { data, profile, auth } }) => {
        const subscription = data.subscriptions && data.subscriptions[auth.uid]
        return {
            auth,
            plans: data.plans && data.plans,
            subscription: subscription,
            isPremium: subscription && subscription.type === 'premium'
        }
    }),
    withStateHandlers(
        {
            events: {
                isLoaded: false
            },
            opening: false,
            playModalVisible: false,
            activeSportEvent: null,
            openingChannelKey: null
        },
        {
            toggleOpening: ({ opening }) => (openingChannelKey = null) => ({
                opening: !opening,
                openingChannelKey: openingChannelKey
            }),
            toggleModalPlayVisible: ({ playModalVisible }) => (
                sportEvent = null,
                show = null
            ) => {
                return {
                    playModalVisible: show !== null ? show : !playModalVisible,
                    activeSportEvent: sportEvent
                }
            }
        }
    ),
    withHandlers({
        openInPlayer: ({
            navigation,
            toggleOpening,
            toggleModalPlayVisible,
            auth,
            plans,
            subscription
        }) => async (openingChannelKey, channelNo, setOpenStripeCheckout) => {
            toggleOpening(openingChannelKey)
            const canPlayResponse = await canPlay({ userId: auth.uid })

            if (canPlayResponse && !canPlayResponse.data.can) {
                const { data } = canPlayResponse
                if (data.type == 'no.valid.susbcription') {
                    handleNotValidSubscription(
                        data,
                        auth,
                        plans,
                        setOpenStripeCheckout
                    )
                }

                if (data.type == 'online.restriction') {
                    handleOnlineRestriction(data)
                }

                toggleOpening(null)
                return
            }

            const open = async () => {
                const res = await getChannelLink(channelNo)
                const link = res.data ? res.data.url.acestream : null
                if (link && Linking.canOpenURL(link)) {
                    toggleOpening(null)
                    toggleModalPlayVisible(null, false)
                    Linking.openURL(link)
                } else {
                    navigation.navigate('RequiredAppsScreen')
                }
            }

            if (subscription.type !== 'premium') {
                await LoadInterstitialAd(async () => {
                    await open()
                })
            } else {
                await open()
            }
        },
        getLocaleDateTime: ({}) => (date, time) => {
            let localeTime = time.replace(' CET','');

            let CETDateTime =  moment.tz(date+" "+localeTime,  'DD/MM/YYYY HH:mm', 'Europe/Madrid');
            let localDateTime = CETDateTime.tz(Localization.timezone);

            return {
                date: localDateTime.format("DD/MM/YYYY"),
                time: localDateTime.format("HH:mm"),
            }
        }
    }),
    lifecycle({
        componentDidMount() {
            const sport = this.props.navigation.getParam('sport')
            getGuide({ sport }).then(response => {
                this.setState({ events: response.data })
            })
        },
        componentWillUnmount() {
            if (
                this.props.subscription &&
                this.props.subscription.type !== 'premium'
            ) {
                AdMobInterstitial.removeAllListeners()
            }
        }
    }),
    spinnerWhileLoading(['events'])
)
