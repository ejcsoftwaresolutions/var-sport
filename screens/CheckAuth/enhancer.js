import { connect } from 'react-redux'
import { compose } from 'redux'
import {
    firebaseConnect,
    isEmpty,
    isLoaded,
    populate,
    pathToJS
} from 'react-redux-firebase'
import { spinnerWhileLoading } from '@components'

export default compose(
    connect(({ firebase }) => ({
        auth: firebase.auth // gets auth from redux and sets as prop
    })),
    firebaseConnect((state, props) => {
        return state.auth.uid
            ? ['subscriptions/' + state.auth.uid, 'plans']
            : []
    }),
    connect(({ firebase, firebase: { auth, profile, data } }) => {
        let isLogged = isLoaded(auth) && !isEmpty(auth)
        return {
            loggedIn: isLoaded(auth) && !isEmpty(auth),
            auth,
            subscription: data.subscriptions
                ? data.subscriptions[auth.uid]
                : isLoaded(auth) && !isEmpty(auth)
                ? { isLoaded: false, isEmpty: true }
                : null,
            plans: isLoaded(data.plans)
                ? data.plans
                : isLogged
                ? { isLoaded: false, isEmpty: true }
                : null
        }
    }),
    spinnerWhileLoading(['auth', 'subscription', 'plans'])
)
