import React, { Fragment } from 'react'
import enhancer from './enhancer'
import Visitor from '../Visitor'
import LoggedIn from '../LoggedIn'
import RequiredAppsScreen from '../RequiredAppsScreen'
import { createSwitchNavigator, createAppContainer } from 'react-navigation'
import NavigationService from '@services/navigation'
import { Linking, Alert } from 'react-native'
import { Constants } from 'expo'
import { checkPlanLimitations, logOut } from '@services/auth'

/* let CheckAuth = ({ loggedIn }) => (
  <Fragment>{loggedIn ? <LoggedIn /> : <Visitor />}</Fragment>
) */

let getRootNavigator = (loggedIn, requiredAppInstalled) =>
    createAppContainer(
        createSwitchNavigator(
            { Visitor, LoggedIn, RequiredAppsScreen },
            {
                initialRouteName: loggedIn
                    ? requiredAppInstalled
                        ? 'LoggedIn'
                        : 'RequiredAppsScreen'
                    : 'Visitor'
            }
        )
    )

class CheckAuth extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            requiredAppInstalled: true
        }
    }

    async componentDidMount() {
        try {
            let canOpen = await this.checkRequiredAppsAreInstalled()
            this.setState({ requiredAppInstalled: canOpen })
        } catch (e) {
            this.setState({ requiredAppInstalled: false })
        }
        this.checkSubscriptionWarnings()

        const { firebase } = this.props
        const resLimits = await checkPlanLimitations(
            firebase,
            this.props.auth.email
        )
        if (!resLimits.passed) {
            Alert.alert(
                'Maximo de usuarios en linea superado',
                'Tu plan tiene un maximo de usuarios por cuenta',
                [
                    {
                        text: 'Ok',
                        onPress: () => {
                            logOut(firebase)
                        },
                        style: 'cancel'
                    }
                ],
                { cancelable: false }
            )
            return
        }

        if (this.props.loggedIn) {
            this.trackOnlineUsers()
        }
    }

    trackOnlineUsers() {
        const { firebase, auth } = this.props
        const deviceObject = {
            platform: Constants.platform,
            installationId: Constants.installationId,
            deviceName: Constants.deviceName,
            ssId: Constants.sessionId,
            email: auth.email,
            userId: auth.uid
        }

        const accountRef = firebase.database().ref('USERS_ONLINE/' + auth.uid)
        const userRef = accountRef.child(deviceObject.installationId)
        firebase
            .database()
            .ref('.info/connected')
            .on('value', snap => {
                if (snap.val()) {
                    // if we lose network then remove this user from the list
                    userRef.onDisconnect().remove()
                    // set user's online status
                    this.setUserStatus(userRef, deviceObject, 'online')
                }
            })
    }

    setUserStatus(myUserRef, deviceObject, status) {
        myUserRef.set({ ...deviceObject, status: status })
    }

    componentDidUpdate(prevProps) {
        this.checkSubscriptionWarnings()
    }

    async checkSubscriptionWarnings() {
        const { subscription } = this.props

        if (subscription && subscription.lastCancellationReason) {
            const title =
                subscription.lastCancellationReason ===
                'trial_end_payment_failed'
                    ? 'No se pudo activar una suscripción'
                    : 'No se renovó tu suscripción'
            const message =
                subscription.lastCancellationReason ===
                'trial_end_payment_failed'
                    ? 'Tu periodo de prueba culminó e intentamos hacer un cobro a tu tarjeta pero falló luego de varios intentos. Para seguir disfrutando del servicio compra una suscripción.'
                    : 'Intentamos hacer un cobro a tu tarjeta pero falló luego de varios intentos. Tu suscripción no se ha renovado. Para seguir disfrutando del servicio compra una suscripción.'
            Alert.alert(title, message, [
                {
                    text: 'Ok',
                    onPress: () => {
                        try {
                            this.props.firebase
                                .ref('subscriptions/' + this.props.auth.uid)
                                .update({
                                    lastCancellationReason: null
                                })
                        } catch (error) {
                            console.log(error)
                        }
                    },
                    style: 'cancel'
                }
            ])
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextProps.loggedIn !== this.props.loggedIn ||
            nextState.requiredAppInstalled !==
                this.state.requiredAppInstalled ||
            (nextProps.subscription && this.props.subscription
                ? nextProps.subscription.lastCancellationReason !==
                  this.props.subscription.lastCancellationReason
                : false)
        )
    }

    async checkRequiredAppsAreInstalled() {
        return new Promise((resolve, reject) => {
            Linking.canOpenURL('acestream://').then(canOpen => {
                if (!canOpen) {
                    console.log('Acestream not installed!')
                    reject(canOpen)
                } else {
                    console.log('Acestream is installed :D!')
                    resolve(canOpen)
                }
            })
        })
    }

    render() {
        let Nav = getRootNavigator(
            this.props.loggedIn,
            this.state.requiredAppInstalled
        )

        return (
            <>
                <Nav
                    ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef)
                    }}
                />
            </>
        )
    }
}

export default enhancer(CheckAuth)
