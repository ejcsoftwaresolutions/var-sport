import { compose, withHandlers, withStateHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { Alerts } from '@components'

const validateForm = data => {
    const { name, email, password, terms, confirmPassword } = data
    let isInvalid = false

    if (
        name == '' ||
        email == '' ||
        password == '' ||
        confirmPassword == '' ||
        password !== confirmPassword 
    ) {
        isInvalid = true
    }

    if (name == '') Alerts.danger('Por favor, coloca tu nombre')
    else if (email == '') Alerts.danger('Por favor, coloca tu email')
    else if (password == '') Alerts.danger('Por favor, asigna una contraseña')
    else if (confirmPassword == '')
        Alerts.danger('Por favor, verifica la contraseña')
    else if (password != confirmPassword)
        Alerts.danger('Las contraseñas no concuerdan')
    else if (terms == false)
        Alerts.danger('Por favor acepta los términos y condiciones')

    return isInvalid
}

export default compose(
    firebaseConnect(),
    withStateHandlers(
        {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            isLoading: false
        },
        {
            setIsLoading: () => isLoading => ({
                isLoading: isLoading
            }),
            updateForm: () => (field, value) => ({
                [field]: value
            })
        }
    ),
    withHandlers({
        emailSignup: ({
            firebase,
            name,
            email,
            password,
            confirmPassword,
            setIsLoading
        }) => () => {
            const isInvalid = validateForm({
                name,
                email,
                password,
                confirmPassword,
            })
            if (isInvalid) return

            setIsLoading(true)

            firebase
                .createUser({ email, password }, { name, email })
                .then(async newUser => {
                    const userId = firebase.auth().currentUser.uid
                    try {
                        const subscription = await firebase.uniqueSet(
                            'subscriptions/' + userId,
                            {
                                type: 'free',
                                enjoyedTrial: false
                            }
                        )

                        await firebase.updateProfile({
                            subscription: userId
                        })

                        setIsLoading(false)
                    } catch (err) {
                        setIsLoading(false)
                        Alerts.danger(err.message)
                    }
                })
                .catch(err => {
                    setIsLoading(false)
                    Alerts.danger(err.message)
                })
        }
    })
)
