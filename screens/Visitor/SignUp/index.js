import React from 'react'
import {
    Content,
    Form,
    Item,
    Input,
    Thumbnail,
    View,
    Button,
    Text,
    CheckBox,
    Body,
    Left
} from 'native-base'
import { images, GlobalStyles as styles } from '@components'
import { KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import enhancer from './enhancer'
import { PageDivider } from '@theme/base'
import withLoader from '@components/hoc/withLoader'
import { Linking } from 'expo'
const SubmitButton = withLoader(Button)

let SignUp = ({
    navigation: { navigate },
    updateForm,
    emailSignup,
    isLoading,
    terms
}) => (
    <Content
        contentContainerStyle={[
            styles.verticalAlignCenter,
            styles.pageBackground
        ]}>
        <View style={styles.alignCenterItem}>
            <Thumbnail
                style={[styles.size150, { marginTop: 10, marginBottom: 10 }]}
                source={images['logo']}
            />
        </View>
        <KeyboardAvoidingView behavior="padding">
            <Form style={styles.marginH15}>
                <Item regular>
                    <Input
                        placeholder="Nombre"
                        onChangeText={text => updateForm('name', text)}
                        placeholderTextColor="#b9b9b9"
                    />
                </Item>
                <Item regular>
                    <Input
                        placeholder="Email"
                        onChangeText={text => updateForm('email', text)}
                        placeholderTextColor="#b9b9b9"
                        keyboardType="email-address"
                    />
                </Item>
                <Item regular>
                    <Input
                        placeholder="Contraseña"
                        type="password"
                        onChangeText={text => updateForm('password', text)}
                        placeholderTextColor="#b9b9b9"
                        secureTextEntry
                    />
                </Item>
                <Item regular>
                    <Input
                        placeholder="Confirmar contraseña"
                        type="password"
                        onChangeText={text =>
                            updateForm('confirmPassword', text)
                        }
                        placeholderTextColor="#b9b9b9"
                        secureTextEntry
                    />
                </Item>

                <View  style={{ marginTop: 20,  flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center', flexFlow: "wrap" }}>
                        
                    <Text style={{ color: '#b9b9b9', fontSize: 12, marginRight: 2 }}>
                        Al momento de registrarse acepta los{' '}
                    </Text>
                    <TouchableOpacity
                        onPress={() =>
                            Linking.openURL(
                                'https://www.varsports.in/terminos-y-condiciones'
                            )
                        }>
                        <Text style={{ color: 'blue', fontSize: 12 }}>
                            términos y condiciones{' '}
                        </Text>
                    </TouchableOpacity>
                </View>
            </Form>
        </KeyboardAvoidingView>
        <SubmitButton
            isLoading={isLoading}
            rounded
            block
            onPress={emailSignup}
            secondary
            style={[styles.marginH15, { marginTop: 15 }]}>
            <Text style={styles.whiteTxt}>Regístrate</Text>
        </SubmitButton>

        <Button block onPress={() => navigate('Login')} transparent>
            <Text style={styles.whiteTxt}>Iniciar sesión</Text>
        </Button>
    </Content>
)

export default enhancer(SignUp)
