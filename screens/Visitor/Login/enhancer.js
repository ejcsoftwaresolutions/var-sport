import { compose, withHandlers, withStateHandlers } from 'recompose'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { Alerts } from '@components'

export default compose(
    firebaseConnect(),
    withStateHandlers(
        {
            email: '',
            password: '',
            isLoading: false
        },
        {
            setIsLoading: () => isLoading => ({
                isLoading: isLoading
            }),
            updateForm: () => (field, value) => ({
                [field]: value
            })
        }
    ),
    withHandlers({
        emailLogin: ({
            firebase,
            email,
            password,
            setIsLoading
        }) => async () => {
            setIsLoading(true)

            return firebase
                .login({ email, password })
                .then(data => {
                    setIsLoading(false)
                })
                .catch(err => {
                    setIsLoading(false)
                    Alerts.danger(err.message)
                })
        }
    })
)
