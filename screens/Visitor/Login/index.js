import React from 'react'
import {
    Content,
    Form,
    Item,
    Input,
    Button,
    Text,
    Thumbnail,
    View
} from 'native-base'
import { images, GlobalStyles as styles } from '@components'
import enhancer from './enhancer'
import { KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import { PageDivider, CenteredView } from '@theme/base'
import withLoader from '@components/hoc/withLoader'

const LoginButton = withLoader(Button)

let Login = ({
    navigation: { navigate },
    updateForm,
    isLoading,
    emailLogin
}) => (
    <Content
        contentContainerStyle={[
            styles.verticalAlignCenter,
            styles.pageBackground
        ]}>
        <View style={styles.alignCenterItem}>
            <Thumbnail
                style={[styles.size150, { marginTop: 10, marginBottom: 10 }]}
                source={images['logo']}
            />
        </View>
        <KeyboardAvoidingView behavior="padding">
            <Form style={styles.marginH15}>
                <Item regular style={styles.formItem}>
                    <Input
                        placeholder="Email"
                        onChangeText={text => updateForm('email', text)}
                        placeholderTextColor="#b9b9b9"
                        keyboardType="email-address"
                    />
                </Item>

                <Item regular style={styles.formItem}>
                    <Input
                        placeholder="Contraseña"
                        type="password"
                        onChangeText={text => updateForm('password', text)}
                        placeholderTextColor="#b9b9b9"
                        secureTextEntry
                    />
                </Item>
            </Form>
        </KeyboardAvoidingView>
        <Button
            transparent
            dark
            onPress={() => navigate('ForgotPassword')}
            style={styles.alignCenterSelf}>
            <Text note uppercase={false}>
                Olvidé mi contraseña
            </Text>
        </Button>
        <LoginButton
            rounded
            isLoading={isLoading}
            secondary
            block
            onPress={emailLogin}
            style={[styles.marginH15, { marginTop: 15 }]}>
            <Text style={styles.whiteTxt}>Inicia sesión</Text>
        </LoginButton>

        <PageDivider />

        <CenteredView>
            <Text style={{ color: 'white' }}>No tienes cuenta?</Text>
            <TouchableOpacity onPress={() => navigate('SignUp')}>
                <Text primary>Crear nueva cuenta</Text>
            </TouchableOpacity>
        </CenteredView>
    </Content>
)

export default enhancer(Login)
