import { createStackNavigator } from 'react-navigation'
import Login from './Login'
import Welcome from './Welcome'
import SignUp from './SignUp'
import ForgotPassword from './ForgotPassword'

export default createStackNavigator(
  {
    Login,
    SignUp,
    ForgotPassword,
    Welcome
  },
  {
    initialRouteName: 'Welcome',
    headerMode: 'none'
  }
)
