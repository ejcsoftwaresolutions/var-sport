import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components/native'
import Walkthrough from '@components/walkthrough/Walkthrough'
import PaginationIndicator from '@components/walkthrough/PaginationIndicator'
import images from '@components/images'
import { Slide1, Slide2, Slide3 } from '@components/walkthrough/slides'
import { Button, Text } from 'native-base'
import {
    WalkthoughContainer,
    AuthButtonsContainer,
    AuthLogo
} from '@theme/base'
import { ImageBackground } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid'

export default class Welcome extends Component {
    constructor(props) {
        super(props)

        this.state = {
            index: 0
        }

        this.changeSliderIndex = this.changeSliderIndex.bind(this)
    }

    changeSliderIndex(index) {
        this.setState({ index })
    }

    render() {
        return (
            <ImageBackground
                source={images.coverBackground}
                style={{ width: '100%', height: '100%' }}>
                <WalkthoughContainer>
                    <AuthLogo source={images.logo} />
                    <Walkthrough
                        onChanged={index => this.changeSliderIndex(index)}>
                        <Slide1 />
                        <Slide2 />
                        <Slide3 />
                    </Walkthrough>
                    <PaginationIndicator
                        length={3}
                        current={this.state.index}
                    />

                    <AuthButtonsContainer style={{ height: 45, width: '100%' }}>
                        <Row>
                            <Col
                                style={{
                                    borderRightWidth: 1,
                                    borderRightColor: 'white'
                                }}>
                                <Button
                                    full
                                    success
                                    onPress={() =>
                                        this.props.navigation.navigate('SignUp')
                                    }>
                                    <Text style={{ color: 'white' }}>
                                        Regístrate
                                    </Text>
                                </Button>
                            </Col>
                            <Col>
                                <Button
                                    full
                                    success
                                    onPress={() =>
                                        this.props.navigation.navigate('Login')
                                    }>
                                    <Text style={{ color: 'white' }}>
                                        Iniciar sesión
                                    </Text>
                                </Button>
                            </Col>
                        </Row>
                    </AuthButtonsContainer>
                </WalkthoughContainer>
            </ImageBackground>
        )
    }
}
