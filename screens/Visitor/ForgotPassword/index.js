import React from 'react'
import {
  Content,
  Form,
  Item,
  Input,
  Thumbnail,
  View,
  Button,
  Text
} from 'native-base'
import { KeyboardAvoidingView } from 'react-native'
import { images, GlobalStyles as styles } from '@components'
import enhancer from './enhancer'
import { PageDivider } from "@theme/base"
import withLoader from "@components/hoc/withLoader"

const SubmitButton = withLoader(Button)

let ForgotPassword = ({
  navigation: { navigate },
  updateForm,
  isLoading,
  forgotPassword
}) => (
    <Content
      contentContainerStyle={[styles.verticalAlignCenter, styles.pageBackground]}>
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.alignCenterItem}>
          <Text style={styles.pageHeading}>Recuperar contraseña</Text>
        </View>
        <View style={styles.alignCenterItem}>
          <Thumbnail style={[styles.size150, { marginTop: 10, marginBottom: 10 }]} source={images['logo']} />
        </View>

        <Form style={styles.marginH15}>
          <Item regular style={styles.formItem}>
            <Input
              placeholder="Escribe tu email"
              onChangeText={text => updateForm('email', text)}

              placeholderTextColor="#b9b9b9"
            />
          </Item>
        </Form>
      </KeyboardAvoidingView>
      <SubmitButton
        isLoading={isLoading}
        block
        style={[styles.marginH15, { marginTop: 15 }]}
        rounded
        onPress={forgotPassword}
      >
        <Text style={styles.whiteTxt}>Enviar correo</Text>
      </SubmitButton>


      <Button
        style={styles.marginH15}
        block
        onPress={() => navigate('Login')}
        transparent
      >
        <Text style={styles.whiteTxt}>
          Iniciar sesión
      </Text>
      </Button>
    </Content>
  )
export default enhancer(ForgotPassword)
