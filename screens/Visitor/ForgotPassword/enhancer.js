import { compose, withHandlers, withStateHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { Alerts } from '@components'

export default compose(
  firebaseConnect(),
  withStateHandlers(
    {
      email: '',
      isLoading: false
    },
    {
      setIsLoading: (isLoading) => ({
        isLoading: isLoading
      }),
      updateForm: () => (field, value) => ({
        [field]: value
      })
    }
  ),
  withHandlers({
    forgotPassword: ({ firebase, email, setIsLoading }) => () => {
      setIsLoading(true)
      firebase
        .resetPassword(email)
        .then(() => {
          setIsLoading(false)
          Alerts.msg('Te hemos enviado un correo con las instrucciones.')
        }).catch(e => setIsLoading(false))
    }
  })
)
