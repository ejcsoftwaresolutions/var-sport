import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from '@components'
import { Alerts } from '@components'
import { compose, withHandlers } from 'recompose'

export default compose(
    firebaseConnect(),
    connect(({ firebase: { profile } }) => ({
        profile
    })),
    withHandlers({
        logout: ({ firebase }) => () =>
            firebase.logout().catch(err => Alerts.danger(err.message))
    }),
    spinnerWhileLoading(['profile'])
)
