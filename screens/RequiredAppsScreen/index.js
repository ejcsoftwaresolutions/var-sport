import React from 'react'
import {
    Content,
    Form,
    Item,
    Input,
    Button,
    Text,
    Thumbnail,
    View,
    List,
    Left,
    Right,
    Body,
    ListView
} from 'native-base'
import { images, GlobalStyles as styles } from '@components'
import enhancer from './enhancer'
import withLoader from '@components/hoc/withLoader'
import { FontAwesome } from '@expo/vector-icons'
import {
    RequiredSoftwareListItem,
    RequiredSoftwareListItemText
} from '@theme/base'
import { Linking } from 'react-native'

const SubmitButton = withLoader(Button)

const installApp = () => {
    Linking.openURL(
        `https://www.dropbox.com/s/mytvbm3oict4tfh/ace_stream_engine_v3.1.40.0_org.acestream.core.apk?dl=1`
    )
        .then(() => {})
        .catch(e => console.log('No fue posible instalar app'))
}

let RequiredAppsScreen = ({ navigation: { navigate }, logout }) => (
    <Content
        contentContainerStyle={[
            styles.verticalAlignCenter,
            styles.pageBackground
        ]}>
        <View
            style={{
                marginBottom: 10,
                alignSelf: 'center',
                color: 'white',
                marginTop: 15
            }}>
            <FontAwesome name="exclamation-circle" size={30} color={'white'} />
        </View>
        <Text
            style={{
                fontSize: 24,
                marginBottom: 10,
                alignSelf: 'center',
                color: 'white'
            }}>
            Software requerido
        </Text>
        <Text note style={{ alignSelf: 'center', textAlign: 'center' }}>
            Para continuar, necesitas decargar alguno de los siguientes
            reproductores.
        </Text>

        <View
            style={{
                alignSelf: 'center',
                width: '90%',
                flex: 1,
                marginTop: 20
            }}>
            <List>
                <RequiredSoftwareListItem>
                    <Left style={{}}>
                        <RequiredSoftwareListItemText>
                            AceStream
                        </RequiredSoftwareListItemText>
                    </Left>
                    <Right style={{ flex: 0.8 }}>
                        <Button small rounded onPress={() => installApp()}>
                            <Text>Instalar</Text>
                        </Button>
                    </Right>
                </RequiredSoftwareListItem>
            </List>
        </View>
        <Text note style={{ alignSelf: 'center', textAlign: 'center' }}>
            Cierra e inicia sesión nuevamente cuando tengas todos los requisitos
            para continuar.
        </Text>

        <Button
            rounded
            onPress={logout}
            style={[
                styles.alignCenterSelf,
                { marginTop: 10, marginBottom: 10 }
            ]}>
            <Text uppercase={false}>Cerrar sesión</Text>
        </Button>
    </Content>
)

export default enhancer(RequiredAppsScreen)
