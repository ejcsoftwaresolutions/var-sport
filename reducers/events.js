export const CONSTANTS = {
  FETCH_GUIDE: 'FETCH_GUIDE'
}

export const initialState = {
  events: []
}

const Events = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.FETCH_GUIDE:
      return { ...state, events: action.events }
    default:
      return state
  }
}

export default Events
