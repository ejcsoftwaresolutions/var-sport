/* eslint-disable prettier/prettier */
import styled from 'styled-components/native'
import { Text, ListItem } from 'native-base'
import { Image } from 'react-native'

export const BaseAppContainer = styled.View`
    flex: 1;
    backgroundColor: white
`

export const AuthButtonsContainer = styled.View`
    marginTop: 15;
    display: flex;
    backgroundColor: red;
    justifyContent: flex-end;
`
export const PageDivider = styled.View`
    borderBottomColor: ${props => props.theme.PRIMARY};
    borderBottomWidth: 1;
    marginVertical: 25;
`
export const AuthLogo = styled(Image)`
    width: 150;
    height: 150;
    resizeMode: contain;
    marginTop: 20;
    marginBottom: 20;
    marginLeft: -10;
`

export const WalkthoughContainer = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: ${props => props.theme.PRIMARY_TRANSPARENT}
`

export const WalkthroughSlide = styled.View`
    flex: 1;
  justifyContent: center;
  alignItems: center;
`

export const WalkthroughSlideTitle = styled.Text`
    color: white;
    fontSize: 30;
    marginBottom: 20;
    marginRight: 20;
    marginLeft: 20;
    textAlign: center;
`

export const WalkthroughSlideDescription = styled.Text`
    marginLeft: 10;
    marginRight: 10;
    textAlign: center;
    color: ${props => props.theme.DARK_GREY}
`

export const WalkThroughIndicatorContainer = styled.View`
    flexDirection: row
`

export const WalkthroughIndicator = styled.View`
    width: 10px;
    height: 10px;
    borderRadius: 3px;
    borderColor: #FFF;
    borderWidth: 1px;
    marginHorizontal: 5px;
    backgroundColor:  ${props => props.selected ? "white" : "transparent"};
`

export const CenteredView = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
    alignSelf: center
`

export const RequiredSoftwareListItem = styled(ListItem)`
    backgroundColor: ${props => props.theme.PRIMARY_OPACITY}
    borderRadius: 10;
    padding: 8px;
    borderBottomColor: transparent;
    marginLeft: 0;
`

export const RequiredSoftwareListItemText = styled.Text`
    color: ${props => props.theme.DARK_GREY};
`
