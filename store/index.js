import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import {
    reactReduxFirebase,
    firebaseReducer,
    getFirebase
} from 'react-redux-firebase'
import thunk from 'redux-thunk'

import Reactotron from '../ReactotronConfig'
import firebase from 'firebase'
import { firebaseConfig } from '../config'
import Events, { initialState as eventsInitialState } from '../reducers/events'

const rrfConfig = {
    userProfile: 'users',
    resetBeforeLogin: true,
    enableLogging: false,
    sessions: null,
    profileParamsToPopulate: [{ child: 'subscription', root: 'subscriptions' }]
}

const initialState = {
    events: eventsInitialState
}

const rootReducer = combineReducers({
    firebase: firebaseReducer,
    events: Events
})

firebase.initializeApp(firebaseConfig)

const store = createStore(
    rootReducer,
    initialState,

    compose(
        applyMiddleware(
            thunk.withExtraArgument(getFirebase) // Pass getFirebase function as extra argument
        ),
        reactReduxFirebase(firebase, rrfConfig),
        Reactotron.createEnhancer()
    )
)

export default store
