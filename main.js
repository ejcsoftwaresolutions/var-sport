import Reactotron from './ReactotronConfig'
import { registerRootComponent } from 'expo'

import App from './App'

registerRootComponent(App)
