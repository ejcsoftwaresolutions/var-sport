export const getSubscriptionInfo = subscription => {
    if (
        !subscription ||
        typeof subscription === 'undefined' ||
        !subscription.startDate
    )
        return {
            remaining: 0,
            percentageRemaining: 0
        }

    const oneDay = 24 * 60 * 60 * 1000
    const startDate = new Date(subscription.startDate)
    const endDate = new Date(subscription.endDate)
    const today = new Date()
    const maxDays = Math.ceil(
        (endDate.getTime() - startDate.getTime()) / oneDay
    )

    const diffDays = Math.ceil((today.getTime() - startDate.getTime()) / oneDay)

    const remaining = maxDays - diffDays
    return {
        remaining: remaining,
        percentageRemaining: (diffDays * 100) / maxDays / 100
    }
}
