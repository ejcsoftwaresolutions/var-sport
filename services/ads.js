import { Constants, AdMobInterstitial } from 'expo'

export const LoadInterstitialAd = async callback => {
    // Display an interstitial
    //AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712')
    
    AdMobInterstitial.setAdUnitID('ca-app-pub-6826295524219358/6219363887')
    AdMobInterstitial.setTestDeviceID(
        //'EMULATOR'
        Constants.isDevice ? Constants.deviceId : 'EMULATOR'
    )

    try {
        await AdMobInterstitial.requestAdAsync()
        AdMobInterstitial.addEventListener('interstitialDidClose', callback)
        await AdMobInterstitial.showAdAsync()
    } catch (error) {
        callback()
        console.log(error)
    }
}
