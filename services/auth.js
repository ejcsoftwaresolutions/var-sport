import { Constants } from 'expo'

export const logOut = firebase => {
    return new Promise(async (resolve, reject) => {
        const userId = firebase.auth().currentUser.uid
        const accountRef = firebase.database().ref('USERS_ONLINE/' + userId)
        const userRef = accountRef.child(Constants.installationId)
        try {
            await userRef.remove()
        } catch (error) {
            reject(error)
            return
        }

        firebase
            .logout()
            .then(() => resolve())
            .catch(error => reject(error))
    })
}

export const checkPlanLimitations = async (firebase, email) => {
    const userSnap = await firebase
        .ref('users')
        .orderByChild('email')
        .equalTo(email)
        .once('value')

    const user = userSnap.val()
    if (!user) return { passed: true }
    const userId = Object.keys(user)[0]

    const subscriptionSnap = await firebase
        .ref('subscriptions/' + userId)
        .once('value')
    let subscription = subscriptionSnap.val()
    if (!user) return { passed: true }

    const planSnap = await firebase
        .ref('plans/' + subscription.type)
        .once('value')
    let plan = planSnap.val()
    if (!plan) return { passed: true }

    const planMaxUsers =
        plan.maxOnlineUsers >= 0 ? plan.maxOnlineUsers : Infinity

    const userOnlineWithEmailSnap = await firebase
        .ref('USERS_ONLINE/' + userId)
        .once('value')
    const userOnlineWithEmail = userOnlineWithEmailSnap.val()
    const totalOnline = Object.keys(userOnlineWithEmail || {}).length
    const passed = totalOnline + 1 <= planMaxUsers

    return {
        passed: passed,
        msg: !passed ? 'Hay muchos usuarios conectados' : null
    }
}
