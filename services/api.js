import { serverHost } from '@config'
import axios from 'axios'

const instance = axios.create({
    baseURL: serverHost,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'Access-Control-Allow-Origin': '*'
    },
    withCredentials: false,
    proxy: false
})

export const getGuide = filters => {
    return instance.get('/getGuide', { params: filters })
}

export const getChannelLink = channelNo => {
    return instance.get('/getChannelLink', { params: { ch: channelNo } })
}

export const activateTrial = params => {
    return instance.post('subscriptions/activateTrial', {
        ...params
    })
}

export const subscribe = params => {
    return instance.post('subscriptions/subscribe', {
        ...params
    })
}

export const canPlay = params => {
    return instance.post('/canPlay', { ...params })
}

export const unsubscribe = params => {
    return instance.post('subscriptions/unsubscribe', {
        ...params
    })
}
