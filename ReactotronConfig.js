import Reactotron, {
    networking,
    trackGlobalErrors
} from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'
const reactotron = Reactotron.configure({ host: '192.168.0.5', port: 9090 }) // controls connection & communication settings
    .use(reactotronRedux())
    .use(trackGlobalErrors())

    .useReactNative() // add all built-in react native plugins
    .use(
        networking({
            ignoreContentTypes: /^(image)\/.*$/i,
            ignoreUrls: /\/(logs|symbolicate)$/
        })
    )

if (__DEV__) {
    reactotron.connect()
    console.tron = reactotron
}

export default reactotron
