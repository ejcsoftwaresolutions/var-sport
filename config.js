export let firebaseConfig = {
    apiKey: 'AIzaSyCeiiuHDm1oCZHnUMrVyn7uS31abiGQlj0',
    authDomain: 'chatwi-554c9.firebaseapp.com',
    databaseURL: 'https://chatwi-554c9.firebaseio.com',
    projectId: 'chatwi-554c9',
    storageBucket: 'chatwi-554c9.appspot.com',
    messagingSenderId: '308175618652',
    appId: '1:308175618652:web:5b1b16e5ddf33388'
}

export const serverHost = 'https://var-sport-server.herokuapp.com/'
// export const serverHost = 'http://192.168.0.5:3000/'

export const stripePublicKey = 'pk_live_0E11JhxRd48avmPknDb8a6ei'

var appInfo = {}
appInfo.version = '0.1'
appInfo.links = {}
appInfo.privacyPolicyUrl = ''
appInfo.aboutusUrl = ''
appInfo.termsUrl = ''
appInfo.websiteUrl = ''

export { appInfo }