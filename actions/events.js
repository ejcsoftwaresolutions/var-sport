import { CONSTANTS } from '../reducers/events'

export function getGuide() {
  return dispatch => {
    dispatch({ type: CONSTANTS.FETCH_GUIDE, events: [] })
  }
}
