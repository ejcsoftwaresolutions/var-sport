import React, { Component } from 'react'
import { WebView, Platform, View, Modal } from 'react-native'
import { PropTypes } from 'prop-types'
import { Spinner, Text, Button } from 'native-base'
import colors from '@theme/colors'

const jsCode = `(function() {
    var originalPostMessage = window.postMessage;
    var patchedPostMessage = function(message, targetOrigin, transfer) {
      originalPostMessage(message, targetOrigin, transfer);
    };
    patchedPostMessage.toString = function() {
      return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
    };
    window.postMessage = patchedPostMessage;
  })();`

class StripeCheckout extends Component {
    constructor() {
        super()
        this.webview = null
        this.state = {
            showPaymentResultScreen: false,
            isProcessing: false,
            paymentSuccess: false,
            paymentResult: {}
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.isOpen !== this.props.isOpen) {
            this.setState({
                showPaymentResultScreen: false,
                isProcessing: false,
                paymentSuccess: false,
                paymentResult: {}
            })
        }
    }

    componentDidMount() {}

    createWebViewRef = webview => {
        this.webview = webview
    }

    onWebViewLoaded = () => {
        this.webview.postMessage('WINDOW_CLOSED', '*')
    }

    renderLoadingScreen() {
        return (
            <View style={{ alignSelf: 'center' }}>
                <Spinner color={colors.SECONDARY} />
                <Text style={{ marginTop: 10, color: 'white' }}>
                    {this.props.loadingMessage}
                </Text>
            </View>
        )
    }

    renderSuccessScreen() {
        return (
            <View style={{ alignSelf: 'center' }}>
                <Text style={{ marginTop: 10, color: 'white' }}>
                    {this.state.paymentResult.msg}
                </Text>
                <View style={{ alignSelf: 'center', marginTop: 20 }}>
                    <Button rounded onPress={this.props.onClose}>
                        <Text>Cerrar</Text>
                    </Button>
                </View>
            </View>
        )
    }

    renderErrorScreen() {
        return (
            <View style={{ alignSelf: 'center' }}>
                <Text style={{ marginTop: 10, color: 'white' }}>
                    {this.state.paymentResult.msg}
                </Text>
                <View style={{ alignSelf: 'center', marginTop: 20 }}>
                    <Button rounded onPress={this.props.onClose}>
                        <Text>Cerrar</Text>
                    </Button>
                </View>
            </View>
        )
    }

    getScreen() {
        const { isProcessing, paymentSuccess } = this.state

        if (isProcessing) return this.renderLoadingScreen()

        return paymentSuccess
            ? this.renderSuccessScreen()
            : this.renderErrorScreen()
    }

    handlePayment = async data => {
        this.setState({
            showPaymentResultScreen: true,
            isProcessing: true
        })
        try {
            const response = await this.props.onPaymentSuccess(data)
            this.setState({
                isProcessing: false,
                paymentSuccess: response.data.result === 'success',
                paymentResult: response.data
            })
        } catch (error) {
            this.setState({
                isProcessing: false,
                paymentSuccess: false,
                paymentResult: 'Hubo un error desconocido'
            })
        }
    }

    render() {
        const {
            publicKey,
            amount,
            allowRememberMe,
            currency,
            description,
            imageUrl,
            storeName,
            prepopulatedEmail,
            style,
            onClose,
            isOpen,
            panelLabel
        } = this.props

        const buttonLabel = panelLabel || 'Pagar'
        return (
            <Modal
                animationType="fade"
                transparent
                visible={isOpen}
                onRequestClose={() => false}>
                {this.state.showPaymentResultScreen ? (
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: colors.PRIMARY
                        }}>
                        {this.getScreen()}
                    </View>
                ) : (
                    <WebView
                        ref={this.createWebViewRef}
                        javaScriptEnabled
                        scrollEnabled={false}
                        bounces={false}
                        injectedJavaScript={jsCode}
                        source={{
                            html: `<script src="https://checkout.stripe.com/checkout.js"></script>
                                   <script>
                                       var submittedForm = false;
                                       var handler = StripeCheckout.configure({
                                           key: '${publicKey}',
                                           image: '${imageUrl}',
                                           locale: 'es',
                                           token: function(token) {
                                               submittedForm = true 
                                               window.postMessage(token.id, token.id);
                                           },
                                       });
                                       window.onload = function() {
                                           
                                           handler.open({
                                               image: '${imageUrl}',
                                               name: '${storeName}',
                                               description: '${description}',
                                               amount: ${amount},
                                               currency: '${currency}',
                                               allowRememberMe: ${allowRememberMe},
                                               email: '${prepopulatedEmail}',
                                               panelLabel: '${buttonLabel}',
                                               closed: function(e) {
                                                   if(!submittedForm) {
                                                       window.postMessage("WINDOW_CLOSED", "*");
                                                   }
                                               },
   
                                           });
                                       };
                                       
                                       
                                   </script>`,
                            baseUrl: ''
                        }}
                        onLoadEnd={this.onWebViewLoaded}
                        onMessage={event => {
                            switch (event.nativeEvent.data) {
                                case 'WINDOW_CLOSED':
                                    return onClose()
                                default:
                                    return this.handlePayment(
                                        event.nativeEvent.data
                                    )
                            }
                        }}
                        style={[{ flex: 1 }, style]}
                        scalesPageToFit={Platform.OS === 'android'}
                    />
                )}
            </Modal>
        )
    }
}

StripeCheckout.defaultProps = {
    prepopulatedEmail: '',
    currency: 'USD'
}

export default StripeCheckout
