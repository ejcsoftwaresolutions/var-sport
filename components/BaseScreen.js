import React from 'react'
import { Content, View } from 'native-base'
import colors from '@theme/colors'
import { ScrollView } from 'react-native'
import GoPremiumAd from '@components/GoPremiumAd'

const BaseScreen = props => (
    <View style={{ flex: 1 }}>
        <Content
            contentContainerStyle={{
                backgroundColor: colors.PRIMARY,
                flex: 1
            }}>
            <ScrollView>{props.children}</ScrollView>
            {props.banner && props.banner()}
        </Content>
        <GoPremiumAd />
    </View>
)

export default BaseScreen
