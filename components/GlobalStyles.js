import { StyleSheet } from 'react-native'
import colors from '@theme/colors'

const styles = StyleSheet.create({
    verticalAlignCenter: {
        justifyContent: 'center',
        flex: 1
    },
    pageBackground: {
        backgroundColor: colors.PRIMARY
    },
    alignCenterItem: {
        alignItems: 'center'
    },
    alignCenterSelf: {
        alignSelf: 'center'
    },
    pageHeading: {
        fontSize: 25,
        color: 'white'
    },
    size150: {
        width: 150,
        height: 150
    },
    size50: {
        width: 50,
        height: 50
    },
    marginH15: {
        marginHorizontal: 15
    },
    marginV15: {
        marginVertical: 15
    },
    subHeading: {
        marginBottom: 10
    },
    whiteTxt: {
        color: '#fff'
    },
    secondaryText: {
        color: colors.SECONDARY
    },
    pageTitle: {
        fontSize: 25,
        fontWeight: 'bold'
    }
})

export default styles
