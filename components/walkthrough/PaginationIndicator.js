import React from 'react';
import {
    StyleSheet,
    View,
    Text
} from 'react-native';

import { WalkThroughIndicatorContainer, WalkthroughIndicator} from "@theme/base"

import styled from 'styled-components/native';


export default class PaginationIndicator extends React.Component {
    constructor(props) {
        super(props);
    }

    _renderItem(index, selected) {
        return (
            <WalkthroughIndicator key={index} selected={selected}/>
        )
    }

    _renderIndicators() {
        let length = this.props.length;
        let current = this.props.current;

        let indicators = [];
        for (let i = 0; i < length; i++) {
            indicators.push(this._renderItem(i, i === current))
        }

        return indicators
    }

    render() {
        return (
          <WalkThroughIndicatorContainer>
            {this._renderIndicators()}
          </WalkThroughIndicatorContainer>
        )
    }
}
