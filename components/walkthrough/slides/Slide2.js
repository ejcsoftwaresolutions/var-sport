import React, { Component } from 'react'
import {
    WalkthroughSlide,
    WalkthroughSlideTitle,
    WalkthroughSlideDescription
} from '@theme/base'

const Slide2 = () => (
    <WalkthroughSlide>
        <WalkthroughSlideTitle>
            SI QUIERES DISFRUTAR AL MÁXIMO
        </WalkthroughSlideTitle>

        <WalkthroughSlideDescription note>
            Esta es tu aplicación, no te pierdas NADA de tus deportes favoritos
            en HD.
        </WalkthroughSlideDescription>
    </WalkthroughSlide>
)

export default Slide2
