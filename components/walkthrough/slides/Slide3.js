import React, { Component } from 'react'
import {
    WalkthroughSlide,
    WalkthroughSlideTitle,
    WalkthroughSlideDescription
} from '@theme/base'

const Slide3 = () => (
    <WalkthroughSlide>
        <WalkthroughSlideTitle>
            MEJOR QUE OTRAS PLATAFORMAS
        </WalkthroughSlideTitle>

        <WalkthroughSlideDescription note>
            Accede totalmente GRATIS y te quedaras por miles de motivos.
        </WalkthroughSlideDescription>
    </WalkthroughSlide>
)

export default Slide3
