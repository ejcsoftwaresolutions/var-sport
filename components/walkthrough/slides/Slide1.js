import React, { Component } from 'react'
import { Text } from 'native-base'
import {
    WalkthroughSlide,
    WalkthroughSlideTitle,
    WalkthroughSlideDescription
} from '@theme/base'

const Slide1 = () => (
    <WalkthroughSlide>
        <WalkthroughSlideTitle>¿TIENES INTERNET?</WalkthroughSlideTitle>

        <WalkthroughSlideDescription note>
            Si tienes internet tienes, Fútbol, Tenis, Baloncesto, Formula1, Moto
            GP y muchos más deportes en VIVO.
        </WalkthroughSlideDescription>
    </WalkthroughSlide>
)

export default Slide1
