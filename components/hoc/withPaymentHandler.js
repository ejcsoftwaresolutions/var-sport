import React from 'react'
import { View } from 'react-native'
import StripeCheckout from '@components/StripeCheckout'
import { activateTrial, subscribe } from '@api'
import { branch, renderComponent } from 'recompose'
import { stripePublicKey } from '@config'

const stripeConfig = {
    publicKey: stripePublicKey,
    imageUrl: 'https://www.dropbox.com/s/6d99dw5gklnqhze/iconSplash.png?dl=1',
    storeName: 'Var Sport',
    allowRememberMe: false,
    currency: 'EUR'
}

export default Component => {
    return class Wrapper extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                openStripeCheckout: false,
                openPayment: {}
            }
        }
        setOpenStripeCheckout = (open = true, paymentData = {}) =>
            this.setState({
                openStripeCheckout: open,
                openPayment: paymentData
            })

        setOpenPayment = paymentData =>
            this.setState({
                openPayment: paymentData
            })

        render() {
            return (
                <>
                    <StripeCheckout
                        {...stripeConfig}
                        {...this.state.openPayment}
                        onClose={() => {
                            this.setOpenStripeCheckout(false)
                        }}
                        onPaymentSuccess={async token => {
                            const apiMethod = this.state.openPayment.trialTarget
                                ? activateTrial
                                : subscribe

                            return apiMethod({
                                ...this.state.openPayment,
                                token,
                                userId: this.props.auth.uid
                            })
                        }}
                        isOpen={this.state.openStripeCheckout}
                    />
                    <Component
                        {...this.props}
                        setOpenStripeCheckout={this.setOpenStripeCheckout}
                    />
                </>
            )
        }
    }
}
