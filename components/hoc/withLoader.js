import React from "react"
import { View } from "react-native"
import { Spinner } from "native-base"

export default (Button) => {
    return ({ isLoading, children, ...rest }) => (
        <Button {...rest}>

            {isLoading && (
                <View>
                    <Spinner color="white" />
                </View>
            )}
            {!isLoading && (
                children

            )}

        </Button>
    )
}