let images = {
    logo: require('../assets/images/icon.png'),
    football: require('../assets/images/football.png'),
    tenis: require('../assets/images/tenis.png'),
    basket: require('../assets/images/basket.jpg'),
    moto: require('../assets/images/moto.png'),
    formula1: require('../assets/images/formula1.png'),
    coverBackground: require('../assets/images/primaryBackground.jpeg'),
    adBanner: require('../assets/images/adbanner.jpg')
}

export default images
