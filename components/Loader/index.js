import React from 'react'
import { Spinner, Text } from 'native-base'
import { View } from 'react-native'
import colors from '@theme/colors'

export default () => (
    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
        <View style={{ padding: 30 }}>
            <Spinner color={colors.SECONDARY} />
            <Text
                style={{
                    marginTop: 10,
                    fontWeight: 'bold',
                    textAlign: 'center'
                }}>
                Cargando contenido...
            </Text>
            <Text mute style={{ textAlign: 'center' }}>
                Por favor espere... sea paciente. Todo depende de su velocidad
                de conexión
            </Text>
        </View>
    </View>
)
