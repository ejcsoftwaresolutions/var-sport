import React from 'react'
import { List, Content, Thumbnail, View, Text, Button, Icon } from 'native-base'
import { images, GlobalStyles } from '@components'
import enhancer from './enhancer'
import colors from '@theme/colors'
import withPaymentHandler from '@components/hoc/withPaymentHandler'

import {
    ScrollView,
    Image,
    Modal,
    ImageBackground,
    TouchableOpacity
} from 'react-native'

let GoPremiumAd = ({
    auth,
    sports,
    show,
    toggleShow,
    plans,
    setOpenStripeCheckout
}) => (
    <Modal
        animationType="fade"
        transparent
        visible={show}
        onRequestClose={() => false}>
        <View
            style={{
                backgroundColor: colors.PRIMARY,
                flex: 1
            }}>
            <View
                style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    zIndex: 9999
                }}>
                <Button transparent onPress={() => toggleShow(false)}>
                    <Icon name="close" type="AntDesign" />
                </Button>
            </View>

            <ImageBackground
                source={images.adBanner}
                style={{
                    width: '100%',
                    height: 200,
                    color: colors.PRIMARY_OPACITY
                }}>
                <View
                    style={{
                        display: 'flex',
                        height: '100%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: colors.PRIMARY_TRANSPARENT
                    }}>
                    <Text note style={GlobalStyles.whiteTxt}>
                        ¡PASATE A PREMIUM!
                    </Text>
                    <Image
                        style={{
                            width: 95,
                            height: 95,
                            marginBottom: 20,
                            marginTop: 20
                        }}
                        source={images.logo}
                    />
                </View>
            </ImageBackground>

            <View
                style={{
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row',
                    width: '100%',
                    marginBottom: 10
                }}>
                {sports &&
                    Object.values(sports).map(sport => (
                        <View
                            style={{
                                backgroundColor: '#fbfbfb78',
                                borderRadius: 10,
                                marginHorizontal: 5,
                                padding: 5
                            }}>
                            <Image
                                style={{
                                    width: 50,
                                    height: 50
                                }}
                                source={
                                    Object.keys(images).indexOf(
                                        sport.iconConstant
                                    ) > -1
                                        ? images[sport.iconConstant]
                                        : null
                                }
                            />
                            <Text
                                style={{
                                    fontSize: 10,
                                    textAlign: 'center'
                                }}>
                                {sport.name}
                            </Text>
                        </View>
                    ))}
            </View>
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column'
                }}>
                <View
                    style={{
                        height: 150,
                        backgroundColor: colors.PRIMARY_LIGHT_2,
                        borderRadius: 20,
                        padding: 20,
                        marginHorizontal: 20
                    }}>
                    <Text note style={{ textAlign: 'center' }}>
                        Quita los anuncios, carga más rápido , y mira en HD,
                        todo por{' '}
                        <Text bold style={{ color: colors.SECONDARY }}>
                            2,67€/mes
                        </Text>
                        , no te lo pienses, pásate a{' '}
                        <Text bold style={{ color: colors.SECONDARY }}>
                            PREMIUM
                        </Text>
                        .
                    </Text>
                    <View
                        style={{
                            justifyContent: 'center',
                            flexDirection: 'row',
                            marginTop: 10,
                            zIndex: 9999
                        }}>
                        <Button
                            primary
                            rounded
                            onPress={() => {
                                setOpenStripeCheckout(true, {
                                    prepopulatedEmail: auth.email,
                                    amount: plans['premium'].price * 100,
                                    description: 'Plan Premium',
                                    planId: 'premium',
                                    loadingMessage: 'Procesando tu pago...',
                                    panelLabel: 'Pagar'
                                })
                            }}>
                            <Text>PASAR A PREMIUM</Text>
                        </Button>
                    </View>
                </View>
            </View>
        </View>
        <TouchableOpacity
            onPress={() => toggleShow(false)}
            style={{
                position: 'absolute',
                bottom: 10,
                right: 10
            }}>
            <Text note style={{ fontSize: 10 }}>
                No gracias
            </Text>
        </TouchableOpacity>
    </Modal>
)

export default enhancer(withPaymentHandler(GoPremiumAd))
