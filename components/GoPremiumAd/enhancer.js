import { connect } from 'react-redux'
import { compose, lifecycle, withStateHandlers } from 'recompose'
import { firebaseConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import { spinnerWhileLoading } from '@components'

export default compose(
    firebaseConnect((state, props) => {
        return ['sports']
    }),
    connect(({ firebase: { data, auth, profile }, ads }) => {
        const subscription = data.subscriptions && data.subscriptions[auth.uid]

        return {
            auth,
            sports: data.sports && data.sports,
            isLoggedIn: !isEmpty(auth) && isLoaded(auth),
            isPremium: subscription && subscription.type === 'premium',
            plans: data.plans && data.plans
        }
    }),
    withStateHandlers(
        ({ isLoggedIn, isPremium }) => {
            let random = Math.random() >= 0.5

            return {
                show: isLoggedIn && !isPremium && random
            }
        },
        {
            toggleShow: () => show => ({
                show: show
            })
        }
    ),
    lifecycle({
        componentDidMount() {}
    })
)
