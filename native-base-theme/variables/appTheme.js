import material from './material'
import colors from "@theme/colors"


export default {
	...material,
	brandDark: colors.SECONDARY_TEXT_COLOR,
	iconStyle: colors.SECONDARY_TEXT_COLOR,
	textColor: colors.SECONDARY_TEXT_COLOR,
	expandedIconStyle: colors.SECONDARY_TEXT_COLOR,
	brandDark: colors.SECONDARY_TEXT_COLOR,
	datePickerTextColor: colors.SECONDARY_TEXT_COLOR,
	iconColor: colors.SECONDARY_TEXT_COLOR,
	cardBorderRadius: 10,
	sTabBarActiveTextColor: colors.SECONDARY_TEXT_COLOR,
	listNoteColor: colors.SECONDARY_TEXT_COLOR,
	listDividerBg: colors.BLUEGRAY_500,
	labelColor: colors.SECONDARY_TEXT_COLOR,
	textNoteColor: colors.BLUEGRAY_300,
	checkboxBgColor: colors.SECONDARY,
	radioColor: colors.SECONDARY,
	radioSelectedColorAndroid: colors.SECONDARY,
	btnSuccessBg: colors.SECONDARY,
	btnPrimaryBg: colors.SECONDARY,
	btnWarningBg: colors.SECONDARY,
	linkColor: colors.PRIMARY,
	inputBorderColor: "transparent",
	inputColor: colors.PRIMARY_TEXT_COLOR,
	primaryTextColor: colors.SECONDARY,
	inputStyles: {
		borderRadius: 8,
		borderWidth: 1,
		borderColor: "rgba(0,0,0,0.1)",
		backgroundColor: "rgba(255, 255, 255,0.1)"
	},
	itemStyles: {
		marginBottom: 5,
		borderColor: "transparent"
	}
}